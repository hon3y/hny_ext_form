
plugin.tx_hiveextform_hiveextformformrender {
    view {
        templateRootPaths.0 = EXT:hive_ext_form/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextform_hiveextformformrender.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_form/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextform_hiveextformformrender.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_form/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextform_hiveextformformrender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextform_hiveextformformrender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

plugin.tx_hiveextform_hiveextformrenderrender {
    view {
        templateRootPaths.0 = EXT:hive_ext_form/Resources/Private/Templates/
        templateRootPaths.1 = {$plugin.tx_hiveextform_hiveextformrenderrender.view.templateRootPath}
        partialRootPaths.0 = EXT:hive_ext_form/Resources/Private/Partials/
        partialRootPaths.1 = {$plugin.tx_hiveextform_hiveextformrenderrender.view.partialRootPath}
        layoutRootPaths.0 = EXT:hive_ext_form/Resources/Private/Layouts/
        layoutRootPaths.1 = {$plugin.tx_hiveextform_hiveextformrenderrender.view.layoutRootPath}
    }
    persistence {
        storagePid = {$plugin.tx_hiveextform_hiveextformrenderrender.persistence.storagePid}
        #recursive = 1
    }
    features {
        #skipDefaultArguments = 1
        # if set to 1, the enable fields are ignored in BE context
        ignoreAllEnableFieldsInBe = 0
        # Should be on by default, but can be disabled if all action in the plugin are uncached
        requireCHashArgumentForActionArguments = 1
    }
    mvc {
        #callDefaultActionIfActionCantBeResolved = 1
    }
}

# these classes are only used in auto-generated templates
plugin.tx_hiveextform._CSS_DEFAULT_STYLE (
    textarea.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    input.f3-form-error {
        background-color:#FF9F9F;
        border: 1px #FF0000 solid;
    }

    .tx-hive-ext-form table {
        border-collapse:separate;
        border-spacing:10px;
    }

    .tx-hive-ext-form table th {
        font-weight:bold;
    }

    .tx-hive-ext-form table td {
        vertical-align:top;
    }

    .typo3-messages .message-error {
        color:red;
    }

    .typo3-messages .message-ok {
        color:green;
    }
)

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextform {
        model {
            HIVE\HiveExtForm\Domain\Model\Render {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\Render.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\Form {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\Form.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\Step {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\Step.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\Fieldset {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\Fieldset.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\Field {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\Field.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\Constraint {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\Constraint.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\MailToSenderConfirm {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\MailToSenderConfirm.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\MailToSender {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\MailToSender.persistence.storagePid}
                }
            }
            HIVE\HiveExtForm\Domain\Model\MailToReceiver {
                persistence {
                    storagePid = {$plugin.tx_hiveextform.model.HIVE\HiveExtForm\Domain\Model\MailToReceiver.persistence.storagePid}
                }
            }
        }

        settings {
            mail {
                to {
                    sender {
                        enable = {$plugin.tx_hiveextform.settings.mail.to.sender.enable}
                        from {
                            name = {$plugin.tx_hiveextform.settings.mail.to.sender.from.name}
                            mail = {$plugin.tx_hiveextform.settings.mail.to.sender.from.mail}
                            subject = {$plugin.tx_hiveextform.settings.mail.to.sender.from.subject}
                        }
                        replyTo {
                            name = {$plugin.tx_hiveextform.settings.mail.to.sender.replyTo.name}
                            mail = {$plugin.tx_hiveextform.settings.mail.to.sender.replyTo.mail}
                        }
                        format {
                            html = 0
                            plain = 1
                        }
                    }
                    receiver {
                        enable = {$plugin.tx_hiveextform.settings.mail.to.receiver.enable}
                        from {
                            name = {$plugin.tx_hiveextform.settings.mail.to.receiver.from.name}
                            mail = {$plugin.tx_hiveextform.settings.mail.to.receiver.from.mail}
                            subject = {$plugin.tx_hiveextform.settings.mail.to.receiver.from.subject}
                        }
                        to {
                            name = {$plugin.tx_hiveextform.settings.mail.to.receiver.to.name}
                            mail = {$plugin.tx_hiveextform.settings.mail.to.receiver.to.mail}
                            subject = {$plugin.tx_hiveextform.settings.mail.to.receiver.to.subject}
                            cc = {$plugin.tx_hiveextform.settings.mail.to.receiver.to.cc}
                            bcc = {$plugin.tx_hiveextform.settings.mail.to.receiver.to.bcc}
                        }
                        format {
                            html = 0
                            plain = 1
                        }
                    }
                }
            }

            storage {
                enable = {$plugin.tx_hiveextform.settings.storage.enable}
                absolute_path_from_webroot = {$plugin.tx_hiveextform.settings.storage.absolute_path_from_webroot}
                confirm {
                    absolute_path_from_webroot = {$plugin.tx_hiveextform.settings.storage.confirm.absolute_path_from_webroot}
                }
            }

            validation {
                captcha {
                    reCaptcha{
                        sitekey = {$plugin.tx_hiveextform.settings.validation.captcha.reCaptcha.sitekey}
                    }
                }
            }

        }

    }
}