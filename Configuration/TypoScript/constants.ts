
plugin.tx_hiveextform_hiveextformformrender {
    view {
        # cat=plugin.tx_hiveextform_hiveextformformrender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_form/Resources/Private/Templates/
        # cat=plugin.tx_hiveextform_hiveextformformrender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_form/Resources/Private/Partials/
        # cat=plugin.tx_hiveextform_hiveextformformrender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_form/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextform_hiveextformformrender//a; type=string; label=Default storage PID
        storagePid =
    }
}

plugin.tx_hiveextform_hiveextformrenderrender {
    view {
        # cat=plugin.tx_hiveextform_hiveextformrenderrender/file; type=string; label=Path to template root (FE)
        templateRootPath = EXT:hive_ext_form/Resources/Private/Templates/
        # cat=plugin.tx_hiveextform_hiveextformrenderrender/file; type=string; label=Path to template partials (FE)
        partialRootPath = EXT:hive_ext_form/Resources/Private/Partials/
        # cat=plugin.tx_hiveextform_hiveextformrenderrender/file; type=string; label=Path to template layouts (FE)
        layoutRootPath = EXT:hive_ext_form/Resources/Private/Layouts/
    }
    persistence {
        # cat=plugin.tx_hiveextform_hiveextformrenderrender//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

plugin {
    tx_hiveextform {
        model {
            HIVE\HiveExtForm\Domain\Model\Render {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\Form {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\Step {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\Fieldset {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\Field {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\Constraint {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\MailToSenderConfirm {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\MailToSender {
                persistence {
                    storagePid =
                }
            }
            HIVE\HiveExtForm\Domain\Model\MailToReceiver {
                persistence {
                    storagePid =
                }
            }
        }

        settings {
            mail {
                to {
                    sender {
                        enable = 1
                        from {
                            name =
                            mail =
                            subject =
                        }
                        replyTo {
                            name =
                            mail =
                        }
                    }
                    receiver {
                        enable = 1
                        from {
                            name =
                            mail =
                            subject =
                        }
                        to {
                            name =
                            mail =
                            subject =
                            cc =
                            bcc =
                        }
                    }
                }
            }

            storage {
                enable = 1
                ## Ansolute from webroot
                absolute_path_from_webroot = /fileadmin/hive_ext_form/default
                confirm {
                    ## Ansolute from webroot
                    absolute_path_from_webroot = /fileadmin/user_upload/hive_ext_form/default
                }
            }

            validation {
                captcha {
                    reCaptcha_sitekey = xxxxxx
                }
            }
        }
    }
}