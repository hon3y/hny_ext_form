<?php
return [
    'ctrl' => [
        'title' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form',
        'label' => 'title',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'versioningWS' => true,
        'languageField' => 'sys_language_uid',
        'transOrigPointerField' => 'l10n_parent',
        'transOrigDiffSourceField' => 'l10n_diffsource',
        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
            'starttime' => 'starttime',
            'endtime' => 'endtime',
        ],
        'searchFields' => 'title,description,text_submit,text_confirm,field_sender_first_name,field_sender_name,field_sender_mail,storage_path,storage_confirm_path,step,mail_to_sender_confirm,mail_to_sender,mail_to_receiver',
        'iconfile' => 'EXT:hive_ext_form/Resources/Public/Icons/tx_hiveextform_domain_model_form.gif'
    ],
    'interface' => [
        'showRecordFieldList' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, text_submit, text_confirm, field_sender_first_name, field_sender_name, field_sender_mail, storage_path, storage_confirm_path, step, mail_to_sender_confirm, mail_to_sender, mail_to_receiver',
    ],
    'types' => [
        '1' => ['showitem' => 'sys_language_uid, l10n_parent, l10n_diffsource, hidden, title, description, text_submit, text_confirm, field_sender_first_name, field_sender_name, field_sender_mail, storage_path, storage_confirm_path, step, mail_to_sender_confirm, mail_to_sender, mail_to_receiver, --div--;LLL:EXT:frontend/Resources/Private/Language/locallang_ttc.xlf:tabs.access, starttime, endtime'],
    ],
    'columns' => [
        'sys_language_uid' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.language',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'special' => 'languages',
                'items' => [
                    [
                        'LLL:EXT:lang/locallang_general.xlf:LGL.allLanguages',
                        -1,
                        'flags-multiple'
                    ]
                ],
                'default' => 0,
            ],
        ],
        'l10n_parent' => [
            'displayCond' => 'FIELD:sys_language_uid:>:0',
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.l18n_parent',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'items' => [
                    ['', 0],
                ],
                'foreign_table' => 'tx_hiveextform_domain_model_form',
                'foreign_table_where' => 'AND tx_hiveextform_domain_model_form.pid=###CURRENT_PID### AND tx_hiveextform_domain_model_form.sys_language_uid IN (-1,0)',
            ],
        ],
        'l10n_diffsource' => [
            'config' => [
                'type' => 'passthrough',
            ],
        ],
        't3ver_label' => [
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.versionLabel',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'max' => 255,
            ],
        ],
        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.hidden',
            'config' => [
                'type' => 'check',
                'items' => [
                    '1' => [
                        '0' => 'LLL:EXT:lang/locallang_core.xlf:labels.enabled'
                    ]
                ],
            ],
        ],
        'starttime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.starttime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
            ],
        ],
        'endtime' => [
            'exclude' => true,
            'l10n_mode' => 'mergeIfNotBlank',
            'label' => 'LLL:EXT:lang/locallang_general.xlf:LGL.endtime',
            'config' => [
                'type' => 'input',
                'size' => 13,
                'eval' => 'datetime',
                'default' => 0,
                'range' => [
                    'upper' => mktime(0, 0, 0, 1, 1, 2038)
                ],
            ],
        ],

        'title' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.title',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'description' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.description',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'text_submit' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.text_submit',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'text_confirm' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.text_confirm',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim',
            ],
            'defaultExtras' => 'richtext:rte_transform'
        ],
        'field_sender_first_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.field_sender_first_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'field_sender_name' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.field_sender_name',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'field_sender_mail' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.field_sender_mail',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'storage_path' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.storage_path',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'storage_confirm_path' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.storage_confirm_path',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 15,
                'eval' => 'trim'
            ]
        ],
        'step' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.step',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectMultipleSideBySide',
                'foreign_table' => 'tx_hiveextform_domain_model_step',
                'MM' => 'tx_hiveextform_form_step_mm',
                'size' => 10,
                'autoSizeMax' => 30,
                'maxitems' => 9999,
                'multiple' => 0,
                'wizards' => [
                    '_PADDING' => 1,
                    '_VERTICAL' => 1,
                    'edit' => [
                        'module' => [
                            'name' => 'wizard_edit',
                        ],
                        'type' => 'popup',
                        'title' => 'Edit', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.edit
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_edit.gif',
                        'popup_onlyOpenIfSelected' => 1,
                        'JSopenParams' => 'height=350,width=580,status=0,menubar=0,scrollbars=1',
                    ],
                    'add' => [
                        'module' => [
                            'name' => 'wizard_add',
                        ],
                        'type' => 'script',
                        'title' => 'Create new', // todo define label: LLL:EXT:.../Resources/Private/Language/locallang_tca.xlf:wizard.add
                        'icon' => 'EXT:backend/Resources/Public/Images/FormFieldWizard/wizard_add.gif',
                        'params' => [
                            'table' => 'tx_hiveextform_domain_model_step',
                            'pid' => '###CURRENT_PID###',
                            'setValue' => 'prepend'
                        ],
                    ],
                ],
            ],
            
        ],
        'mail_to_sender_confirm' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.mail_to_sender_confirm',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_hiveextform_domain_model_mailtosenderconfirm',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'mail_to_sender' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.mail_to_sender',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_hiveextform_domain_model_mailtosender',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
        'mail_to_receiver' => [
            'exclude' => false,
            'label' => 'LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hiveextform_domain_model_form.mail_to_receiver',
            'config' => [
                'type' => 'select',
                'renderType' => 'selectSingle',
                'foreign_table' => 'tx_hiveextform_domain_model_mailtoreceiver',
                'minitems' => 0,
                'maxitems' => 1,
            ],
        ],
    
    ],
];
