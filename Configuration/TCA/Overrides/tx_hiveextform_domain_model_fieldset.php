<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sUserFuncModel = 'HIVE\\HiveExtForm\\Domain\\Model\\Fieldset';
$sUserFuncPlugin = 'tx_hiveextform';

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';

/*
 * Type
 */
$sColumn = 'field';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['foreign_table_where'] = ' AND tx_hiveextform_domain_model_field.hidden = 0 AND tx_hiveextform_domain_model_field.deleted = 0 AND tx_hiveextform_domain_model_field.sys_language_uid IN (-1,0)';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['minitems'] = 1;