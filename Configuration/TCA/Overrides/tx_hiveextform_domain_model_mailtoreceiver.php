<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sUserFuncModel = 'HIVE\\HiveExtForm\\Domain\\Model\\MailToReceiver';
$sUserFuncPlugin = 'tx_hiveextform';

/*
 * From name
 */
$sColumn = 'from_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "Contact form";

/*
 * From mail
 */
$sColumn = 'from_mail';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "form@musterfirma.de";

/*
 * From subject
 */
$sColumn = 'from_subject';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "Request via contact form";

/*
 * To name
 */
$sColumn = 'to_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "Contact form";

/*
 * To mail
 */
$sColumn = 'to_mail';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "form@musterfirma.de";

/*
 * To cc
 */
$sColumn = 'to_cc';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "cc@musterfirma.de";

/*
 * To bcc
 */
$sColumn = 'to_bcc';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "bcc@musterfirma.de";

/*
 * append data
 */
$sColumn = 'append_data';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';