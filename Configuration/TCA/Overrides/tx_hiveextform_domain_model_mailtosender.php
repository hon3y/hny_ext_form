<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sUserFuncModel = 'HIVE\\HiveExtForm\\Domain\\Model\\MailToSender';
$sUserFuncPlugin = 'tx_hiveextform';

/*
 * From name
 */
$sColumn = 'from_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "Musterfirma GmbH";

/*
 * From mail
 */
$sColumn = 'from_mail';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "info@musterfirma.de";

/*
 * From subject
 */
$sColumn = 'from_subject';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "Your request to Musterfirma GmbH";

/*
 * From salutation
 */
$sColumn = 'from_salutation';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "Dear Customer,";

/*
 * From body
 */
$sColumn = 'from_body';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>thank you for your request.<br><br>Your information will be checked carefully and we will contact you within short by e-mail.</p>";

/*
 * From complimentary close
 */
$sColumn = 'from_complimentary_close';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>Best regards,<br>Max Mustermann</p>";

/*
 * From footer
 */
$sColumn = 'from_footer';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['default'] = "<p>Musterfirma GmbH<br><br>Phone: +49 XXXX XXX XXX<br>Fax: +49 XXXX XXX XXXX<br>E-mail: info@musterfirma.de<br><br>Musterfirma GmbH<br>Musterstr. X | D-XXXXX Musterstadt<br>Registergericht: Mustergericht, HRB XXXXX<br>Geschäftsführer: Max Mustermann<br>www.musterfirma.de</p>";

/*
 * ReplyTo name
 */
$sColumn = 'reply_to_name';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "Musterfirma GmbH";

/*
 * ReplyTo mail
 */
$sColumn = 'reply_to_mail';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config']['placeholder'] = "reply.to@musterfirma.de";

/*
 * append data
 */
$sColumn = 'append_data';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';