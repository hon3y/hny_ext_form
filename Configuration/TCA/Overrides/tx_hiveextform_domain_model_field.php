<?php
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

defined('TYPO3_MODE') or die();

$sModel = basename(__FILE__, '.php');
$sTable = basename(__FILE__, '.php');
$sUserFuncModel = 'HIVE\\HiveExtForm\\Domain\\Model\\Field';
$sUserFuncPlugin = 'tx_hiveextform';

/*
 * Icon
 */
$GLOBALS['TCA'][$sModel]['ctrl']['iconfile'] = 'EXT:hive_cpt_brand/Resources/Public/Icons/SVG/hive_16x16.svg';

/*
 * Type
 */
$sColumn = 'type';
// $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_mode'] = 'exclude';
// $GLOBALS['TCA'][$sModel]['columns'][$sColumn]['l10n_display'] = 'defaultAsReadonly';
$GLOBALS['TCA'][$sModel]['columns'][$sColumn]['config'] = [
    'type' => 'select',
    'renderType' => 'selectSingleBox',
    'items' => [
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.Input',
            '--div--'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.TextType',
            'TextType'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.TextareaType',
            'TextareaType'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.EmailType',
            'EmailType'
        ],
//        [
//            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.PasswordType',
//            'PasswordType'
//        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.HiddenType',
            'HiddenType'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.ReadonlyType',
            'ReadonlyType'
        ],

        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.Choice',
            '--div--'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.CheckboxType',
            'CheckboxType'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.ChoiceType',
            'ChoiceType'
        ],
//        [
//            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.RadioType',
//            'RadioType'
//        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.CountryType',
            'CountryType'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.LanguageType',
            'LanguageType'
        ],

//        [
//            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.Optgroup.Date',
//            '--div--'
//        ],
//        [
//            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.DateTimeType',
//            'DateTimeType'
//        ],

        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.File',
            '--div--'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.FileType',
            'FileType'
        ],

//        [
//            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.Optgroup.Button',
//            '--div--'
//        ],
//        [
//            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.ResetType',
//            'ResetType'
//        ],
//        [
//            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
//            $sTable . '.type.SubmitType',
//            'SubmitType'
//        ],

        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Optgroup.Others',
            '--div--'
        ],
        [
            'LLL:EXT:hive_ext_form/Resources/Private/Language/translation_db.xlf:' .
            $sTable . '.type.Html',
            'Html'
        ],
    ],
    'size' => 21,
    'maxitems' => 1,
    'eval' => 'required',
    'onChange' => 'reload'
];