<?php
namespace HIVE\HiveExtForm\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class MailToSenderConfirmTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtForm\Domain\Model\MailToSenderConfirm
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtForm\Domain\Model\MailToSenderConfirm();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromName()
        );
    }

    /**
     * @test
     */
    public function setFromNameForStringSetsFromName()
    {
        $this->subject->setFromName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromMailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromMail()
        );
    }

    /**
     * @test
     */
    public function setFromMailForStringSetsFromMail()
    {
        $this->subject->setFromMail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromMail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromSubjectReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromSubject()
        );
    }

    /**
     * @test
     */
    public function setFromSubjectForStringSetsFromSubject()
    {
        $this->subject->setFromSubject('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromSubject',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromSalutationReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromSalutation()
        );
    }

    /**
     * @test
     */
    public function setFromSalutationForStringSetsFromSalutation()
    {
        $this->subject->setFromSalutation('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromSalutation',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromBodyBeforeLinkReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromBodyBeforeLink()
        );
    }

    /**
     * @test
     */
    public function setFromBodyBeforeLinkForStringSetsFromBodyBeforeLink()
    {
        $this->subject->setFromBodyBeforeLink('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromBodyBeforeLink',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromBodyAfterLinkReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromBodyAfterLink()
        );
    }

    /**
     * @test
     */
    public function setFromBodyAfterLinkForStringSetsFromBodyAfterLink()
    {
        $this->subject->setFromBodyAfterLink('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromBodyAfterLink',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromComplimentaryCloseReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromComplimentaryClose()
        );
    }

    /**
     * @test
     */
    public function setFromComplimentaryCloseForStringSetsFromComplimentaryClose()
    {
        $this->subject->setFromComplimentaryClose('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromComplimentaryClose',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromFooterReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromFooter()
        );
    }

    /**
     * @test
     */
    public function setFromFooterForStringSetsFromFooter()
    {
        $this->subject->setFromFooter('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromFooter',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getReplyToNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getReplyToName()
        );
    }

    /**
     * @test
     */
    public function setReplyToNameForStringSetsReplyToName()
    {
        $this->subject->setReplyToName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'replyToName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getReplyToMailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getReplyToMail()
        );
    }

    /**
     * @test
     */
    public function setReplyToMailForStringSetsReplyToMail()
    {
        $this->subject->setReplyToMail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'replyToMail',
            $this->subject
        );
    }
}
