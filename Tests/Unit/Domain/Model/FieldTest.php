<?php
namespace HIVE\HiveExtForm\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class FieldTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtForm\Domain\Model\Field
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtForm\Domain\Model\Field();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getTypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getType()
        );
    }

    /**
     * @test
     */
    public function setTypeForStringSetsType()
    {
        $this->subject->setType('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'type',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getRichTextLabelReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getRichTextLabel()
        );
    }

    /**
     * @test
     */
    public function setRichTextLabelForStringSetsRichTextLabel()
    {
        $this->subject->setRichTextLabel('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'richTextLabel',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getOptionsReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getOptions()
        );
    }

    /**
     * @test
     */
    public function setOptionsForStringSetsOptions()
    {
        $this->subject->setOptions('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'options',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getHelpTextReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getHelpText()
        );
    }

    /**
     * @test
     */
    public function setHelpTextForStringSetsHelpText()
    {
        $this->subject->setHelpText('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'helpText',
            $this->subject
        );
    }
}
