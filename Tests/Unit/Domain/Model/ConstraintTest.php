<?php
namespace HIVE\HiveExtForm\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ConstraintTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtForm\Domain\Model\Constraint
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtForm\Domain\Model\Constraint();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getMandatoryReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getMandatory()
        );
    }

    /**
     * @test
     */
    public function setMandatoryForBoolSetsMandatory()
    {
        $this->subject->setMandatory(true);

        self::assertAttributeEquals(
            true,
            'mandatory',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMinLengthReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMinLength()
        );
    }

    /**
     * @test
     */
    public function setMinLengthForIntSetsMinLength()
    {
        $this->subject->setMinLength(12);

        self::assertAttributeEquals(
            12,
            'minLength',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMaxLengthReturnsInitialValueForInt()
    {
        self::assertSame(
            0,
            $this->subject->getMaxLength()
        );
    }

    /**
     * @test
     */
    public function setMaxLengthForIntSetsMaxLength()
    {
        $this->subject->setMaxLength(12);

        self::assertAttributeEquals(
            12,
            'maxLength',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMimeTypeReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMimeType()
        );
    }

    /**
     * @test
     */
    public function setMimeTypeForStringSetsMimeType()
    {
        $this->subject->setMimeType('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'mimeType',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getMaxFileSizeReturnsInitialValueForFloat()
    {
        self::assertSame(
            0.0,
            $this->subject->getMaxFileSize()
        );
    }

    /**
     * @test
     */
    public function setMaxFileSizeForFloatSetsMaxFileSize()
    {
        $this->subject->setMaxFileSize(3.14159265);

        self::assertAttributeEquals(
            3.14159265,
            'maxFileSize',
            $this->subject,
            '',
            0.000000001
        );
    }

    /**
     * @test
     */
    public function getMaxFileSizeUnitReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getMaxFileSizeUnit()
        );
    }

    /**
     * @test
     */
    public function setMaxFileSizeUnitForStringSetsMaxFileSizeUnit()
    {
        $this->subject->setMaxFileSizeUnit('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'maxFileSizeUnit',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFormReturnsInitialValueForForm()
    {
        self::assertEquals(
            null,
            $this->subject->getForm()
        );
    }

    /**
     * @test
     */
    public function setFormForFormSetsForm()
    {
        $formFixture = new \HIVE\HiveExtForm\Domain\Model\Form();
        $this->subject->setForm($formFixture);

        self::assertAttributeEquals(
            $formFixture,
            'form',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFieldReturnsInitialValueForField()
    {
        self::assertEquals(
            null,
            $this->subject->getField()
        );
    }

    /**
     * @test
     */
    public function setFieldForFieldSetsField()
    {
        $fieldFixture = new \HIVE\HiveExtForm\Domain\Model\Field();
        $this->subject->setField($fieldFixture);

        self::assertAttributeEquals(
            $fieldFixture,
            'field',
            $this->subject
        );
    }
}
