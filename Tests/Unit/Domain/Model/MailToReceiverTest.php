<?php
namespace HIVE\HiveExtForm\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class MailToReceiverTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtForm\Domain\Model\MailToReceiver
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtForm\Domain\Model\MailToReceiver();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromName()
        );
    }

    /**
     * @test
     */
    public function setFromNameForStringSetsFromName()
    {
        $this->subject->setFromName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromMailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromMail()
        );
    }

    /**
     * @test
     */
    public function setFromMailForStringSetsFromMail()
    {
        $this->subject->setFromMail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromMail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFromSubjectReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getFromSubject()
        );
    }

    /**
     * @test
     */
    public function setFromSubjectForStringSetsFromSubject()
    {
        $this->subject->setFromSubject('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'fromSubject',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getToNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getToName()
        );
    }

    /**
     * @test
     */
    public function setToNameForStringSetsToName()
    {
        $this->subject->setToName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'toName',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getToMailReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getToMail()
        );
    }

    /**
     * @test
     */
    public function setToMailForStringSetsToMail()
    {
        $this->subject->setToMail('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'toMail',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getToCcReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getToCc()
        );
    }

    /**
     * @test
     */
    public function setToCcForStringSetsToCc()
    {
        $this->subject->setToCc('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'toCc',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getToBccReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getToBcc()
        );
    }

    /**
     * @test
     */
    public function setToBccForStringSetsToBcc()
    {
        $this->subject->setToBcc('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'toBcc',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getAppendDataReturnsInitialValueForBool()
    {
        self::assertSame(
            false,
            $this->subject->getAppendData()
        );
    }

    /**
     * @test
     */
    public function setAppendDataForBoolSetsAppendData()
    {
        $this->subject->setAppendData(true);

        self::assertAttributeEquals(
            true,
            'appendData',
            $this->subject
        );
    }
}
