<?php
namespace HIVE\HiveExtForm\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class StepTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtForm\Domain\Model\Step
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtForm\Domain\Model\Step();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFieldsetReturnsInitialValueForFieldset()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getFieldset()
        );
    }

    /**
     * @test
     */
    public function setFieldsetForObjectStorageContainingFieldsetSetsFieldset()
    {
        $fieldset = new \HIVE\HiveExtForm\Domain\Model\Fieldset();
        $objectStorageHoldingExactlyOneFieldset = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneFieldset->attach($fieldset);
        $this->subject->setFieldset($objectStorageHoldingExactlyOneFieldset);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneFieldset,
            'fieldset',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addFieldsetToObjectStorageHoldingFieldset()
    {
        $fieldset = new \HIVE\HiveExtForm\Domain\Model\Fieldset();
        $fieldsetObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $fieldsetObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($fieldset));
        $this->inject($this->subject, 'fieldset', $fieldsetObjectStorageMock);

        $this->subject->addFieldset($fieldset);
    }

    /**
     * @test
     */
    public function removeFieldsetFromObjectStorageHoldingFieldset()
    {
        $fieldset = new \HIVE\HiveExtForm\Domain\Model\Fieldset();
        $fieldsetObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $fieldsetObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($fieldset));
        $this->inject($this->subject, 'fieldset', $fieldsetObjectStorageMock);

        $this->subject->removeFieldset($fieldset);
    }
}
