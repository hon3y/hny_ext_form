<?php
namespace HIVE\HiveExtForm\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class FieldsetTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HIVE\HiveExtForm\Domain\Model\Fieldset
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HIVE\HiveExtForm\Domain\Model\Fieldset();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getTitleReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getTitle()
        );
    }

    /**
     * @test
     */
    public function setTitleForStringSetsTitle()
    {
        $this->subject->setTitle('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'title',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getFieldReturnsInitialValueForField()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getField()
        );
    }

    /**
     * @test
     */
    public function setFieldForObjectStorageContainingFieldSetsField()
    {
        $field = new \HIVE\HiveExtForm\Domain\Model\Field();
        $objectStorageHoldingExactlyOneField = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneField->attach($field);
        $this->subject->setField($objectStorageHoldingExactlyOneField);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneField,
            'field',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addFieldToObjectStorageHoldingField()
    {
        $field = new \HIVE\HiveExtForm\Domain\Model\Field();
        $fieldObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $fieldObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($field));
        $this->inject($this->subject, 'field', $fieldObjectStorageMock);

        $this->subject->addField($field);
    }

    /**
     * @test
     */
    public function removeFieldFromObjectStorageHoldingField()
    {
        $field = new \HIVE\HiveExtForm\Domain\Model\Field();
        $fieldObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $fieldObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($field));
        $this->inject($this->subject, 'field', $fieldObjectStorageMock);

        $this->subject->removeField($field);
    }
}
