<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtForm',
            'Hiveextformformrender',
            [
                'Form' => 'render'
            ],
            // non-cacheable actions
            [
                'Form' => '',
                'Render' => ''
            ]
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'HIVE.HiveExtForm',
            'Hiveextformrenderrender',
            [
                'Render' => 'render'
            ],
            // non-cacheable actions
            [
                'Render' => 'render'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    hiveextformformrender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_form') . 'Resources/Public/Icons/user_plugin_hiveextformformrender.svg
                        title = LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_form_domain_model_hiveextformformrender
                        description = LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_form_domain_model_hiveextformformrender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextform_hiveextformformrender
                        }
                    }
                    hiveextformrenderrender {
                        icon = ' . \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::extRelPath('hive_ext_form') . 'Resources/Public/Icons/user_plugin_hiveextformrenderrender.svg
                        title = LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_form_domain_model_hiveextformrenderrender
                        description = LLL:EXT:hive_ext_form/Resources/Private/Language/locallang_db.xlf:tx_hive_ext_form_domain_model_hiveextformrenderrender.description
                        tt_content_defValues {
                            CType = list
                            list_type = hiveextform_hiveextformrenderrender
                        }
                    }
                }
                show = *
            }
       }'
    );
    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

call_user_func(
    function($extKey)
    {
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
            'mod {
                wizards.newContentElement.wizardItems.plugins.elements.hiveextformformrender >
                wizards.newContentElement.wizardItems.plugins.elements.hiveextformrenderrender >
                wizards.newContentElement.wizardItems {
                    hive {
                        header = Hive
                        after = common,special,menu,plugins,forms
                        elements.hiveextformrenderrender {
                            iconIdentifier = hive_cpt_brand_32x32_svg
                            title = Form
                            description = Divine form
                            tt_content_defValues {
                                CType = list
                                list_type = hiveextform_hiveextformrenderrender
                            }
                        }
                        show := addToList(hiveextformrenderrender)
                    }

                }
            }'
        );

    }, $_EXTKEY
);


/*
 * Hooks
 */
if (\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::isLoaded('realurl')) {
    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['extensionConfiguration']['hive_ext_form'] =
        \HIVE\HiveExtForm\Hooks\RealUrlAutoConfiguration::class . '->addConfig';

    $GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['ext/realurl/class.tx_realurl_autoconfgen.php']['postProcessConfiguration']['hive_ext_form'] =
        \HIVE\HiveExtForm\Hooks\RealUrlAutoConfiguration::class . '->postProcessConfiguration';
}

/*
 * Example Signals
 *
 * Use in own extension!
 */
///** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
//$signalSlotDispatcher = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
//
//$signalSlotDispatcher->connect(
//    \HIVE\HiveExtForm\Controller\RenderController::class,
//    'addFieldToFormOverrideReadonlyType',
//    \HIVE\HiveExtForm\Slot\RenderControllerSlot::class,
//    'addFieldToFormOverrideReadonlyType',
//    FALSE
//);

///** @var \TYPO3\CMS\Extbase\SignalSlot\Dispatcher $signalSlotDispatcher */
//$signalSlotDispatcher1 = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Extbase\SignalSlot\Dispatcher::class);
//
//$signalSlotDispatcher1->connect(
//    \HIVE\HiveExtForm\Controller\RenderController::class,
//    'addFieldToFormOverrideHiddenType',
//    \HIVE\HiveExtForm\Slot\RenderControllerSlot::class,
//    'addFieldToFormOverrideHiddenType',
//    FALSE
//);