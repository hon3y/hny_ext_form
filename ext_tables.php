<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtForm',
            'Hiveextformformrender',
            'hive_ext_form :: Form :: render'
        );

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'HIVE.HiveExtForm',
            'Hiveextformrenderrender',
            'hive_ext_form :: Render :: render'
        );

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hive_ext_form', 'Configuration/TypoScript', 'hive_ext_form');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_form', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_form.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_form');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_field', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_field.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_field');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_fieldset', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_fieldset.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_fieldset');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_step', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_step.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_step');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_render', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_render.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_render');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_constraint', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_constraint.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_constraint');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_mailtosender', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_mailtosender.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_mailtosender');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_mailtosenderconfirm', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_mailtosenderconfirm.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_mailtosenderconfirm');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hiveextform_domain_model_mailtoreceiver', 'EXT:hive_ext_form/Resources/Private/Language/locallang_csh_tx_hiveextform_domain_model_mailtoreceiver.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hiveextform_domain_model_mailtoreceiver');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder

$extensionName = strtolower(\TYPO3\CMS\Core\Utility\GeneralUtility::underscoredToUpperCamelCase($_EXTKEY));
$pluginName = strtolower('Hiveextformrenderrender');
$pluginSignature = $extensionName.'_'.$pluginName;
$TCA['tt_content']['types']['list']['subtypes_excludelist'][$pluginSignature] = 'layout,select_key,pages,recursive';
$TCA['tt_content']['types']['list']['subtypes_addlist'][$pluginSignature] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue($pluginSignature, 'FILE:EXT:'.$_EXTKEY . '/Configuration/FlexForms/Config.xml');