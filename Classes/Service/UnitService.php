<?php

namespace HIVE\HiveExtForm\Service;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Class UnitService
 * @package HIVE\HiveExtForm\Service
 */
class UnitService implements \TYPO3\CMS\Core\SingletonInterface
{

    public function getMultipliersForUnits()
    {
        $aMultiplyByUnit = [
            "G" => intval(1024 * 1024 * 1024),
            "M" => intval(1024 * 1024),
            "K" => intval(1024),
        ];

        return $aMultiplyByUnit;
    }

    public function getMultiplierForGivenUnit($sUnit = '')
    {
        $aMultiplyByUnit = $this->getMultipliersForUnits();

        return $aMultiplyByUnit[$sUnit];
    }

    public function getSizeInBytes($sFileSize = '')
    {
        $fFileSize = floatval(str_replace(['G', 'M', 'K'], ['', '', ''], $sFileSize));
        $sFileSizeSuffix = (strpos($sFileSize, 'G') !== false ? "G" : (strpos($sFileSize, 'M') !== false ? "M" : "K"));
        $iFileSizeInBytes = intval($fFileSize * $this->getMultiplierForGivenUnit($sFileSizeSuffix));

        return $iFileSizeInBytes;
    }
}