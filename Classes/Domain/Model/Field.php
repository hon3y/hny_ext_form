<?php
namespace HIVE\HiveExtForm\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Field
 */
class Field extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * type
     *
     * @var string
     * @validate NotEmpty
     */
    protected $type = '';

    /**
     * richTextLabel
     *
     * @var string
     */
    protected $richTextLabel = '';

    /**
     * options
     *
     * @var string
     */
    protected $options = '';

    /**
     * helpText
     *
     * @var string
     */
    protected $helpText = '';

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Returns the type
     *
     * @return string $type
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * Sets the type
     *
     * @param string $type
     * @return void
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns the richTextLabel
     *
     * @return string $richTextLabel
     */
    public function getRichTextLabel()
    {
        return $this->richTextLabel;
    }

    /**
     * Sets the richTextLabel
     *
     * @param string $richTextLabel
     * @return void
     */
    public function setRichTextLabel($richTextLabel)
    {
        $this->richTextLabel = $richTextLabel;
    }

    /**
     * Returns the options
     *
     * @return string $options
     */
    public function getOptions()
    {
        return $this->options;
    }

    /**
     * Sets the options
     *
     * @param string $options
     * @return void
     */
    public function setOptions($options)
    {
        $this->options = $options;
    }

    /**
     * Returns the helpText
     *
     * @return string $helpText
     */
    public function getHelpText()
    {
        return $this->helpText;
    }

    /**
     * Sets the helpText
     *
     * @param string $helpText
     * @return void
     */
    public function setHelpText($helpText)
    {
        $this->helpText = $helpText;
    }
}
