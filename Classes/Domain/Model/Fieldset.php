<?php
namespace HIVE\HiveExtForm\Domain\Model;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * Fieldset
 */
class Fieldset extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * title
     *
     * @var string
     * @validate NotEmpty
     */
    protected $title = '';

    /**
     * field
     *
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtForm\Domain\Model\Field>
     */
    protected $field = null;

    /**
     * __construct
     */
    public function __construct()
    {
        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     *
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->field = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the title
     *
     * @return string $title
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Sets the title
     *
     * @param string $title
     * @return void
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * Adds a Field
     *
     * @param \HIVE\HiveExtForm\Domain\Model\Field $field
     * @return void
     */
    public function addField(\HIVE\HiveExtForm\Domain\Model\Field $field)
    {
        $this->field->attach($field);
    }

    /**
     * Removes a Field
     *
     * @param \HIVE\HiveExtForm\Domain\Model\Field $fieldToRemove The Field to be removed
     * @return void
     */
    public function removeField(\HIVE\HiveExtForm\Domain\Model\Field $fieldToRemove)
    {
        $this->field->detach($fieldToRemove);
    }

    /**
     * Returns the field
     *
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtForm\Domain\Model\Field> $field
     */
    public function getField()
    {
        return $this->field;
    }

    /**
     * Sets the field
     *
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtForm\Domain\Model\Field> $field
     * @return void
     */
    public function setField(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $field)
    {
        $this->field = $field;
    }
}
