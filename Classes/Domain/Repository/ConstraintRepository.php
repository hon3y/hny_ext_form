<?php
namespace HIVE\HiveExtForm\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Constraints
 */
class ConstraintRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtForm\\Domain\\Model\\Constraint';
        $sUserFuncPlugin = 'tx_hiveextform';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }

    /**
     * @param int $iFormUid
     * @param int $iFieldUid
     */
    public function getOneByFormAndFieldUid(int $iFormUid, int $iFieldUid)
    {
        $query = $this->createQuery();
        $query->matching($query->logicalAnd($query->equals('form', $iFormUid), $query->equals('field', $iFieldUid)));
        return $query->execute();
    }
}
