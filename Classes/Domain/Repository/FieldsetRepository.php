<?php
namespace HIVE\HiveExtForm\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Fieldsets
 */
class FieldsetRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    /**
     * @var array
     */
    protected $defaultOrderings = [
        'sorting' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
    ];

    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtForm\\Domain\\Model\\Fieldset';
        $sUserFuncPlugin = 'tx_hiveextform';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
