<?php
namespace HIVE\HiveExtForm\Domain\Repository;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The repository for Renders
 */
class RenderRepository extends \HIVE\HiveCfgRepository\Domain\Repository\AbstractRepository
{
    public function initializeObject()
    {
        $sUserFuncModel = 'HIVE\\HiveExtForm\\Domain\\Model\\Render';
        $sUserFuncPlugin = 'tx_hiveextform';
        parent::overrideQuerySettings($sUserFuncModel, $sUserFuncPlugin);
    }
}
