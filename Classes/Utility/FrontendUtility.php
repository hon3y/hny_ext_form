<?php
namespace HIVE\HiveExtForm\Utility;

use TYPO3\CMS\Core\Utility\GeneralUtility;


/**
 * Class FrontendUtility
 *
 * @package HIVE\HiveExtForm\Utility
 */
class FrontendUtility extends AbstractUtility
{
    /**
     * Get charset for frontend rendering
     *
     * @return string
     */
    public static function getCharset()
    {
        return self::getTyposcriptFrontendController()->metaCharset;
    }
}