<?php
namespace HIVE\HiveExtForm\Controller;

use Symfony\Component\Form\Forms;
use Symfony\Component\Translation\Loader\XliffFileLoader;
use Symfony\Component\Translation\Translator;
use Symfony\Component\Translation\Loader\ArrayLoader;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Form\Extension\HttpFoundation\HttpFoundationExtension;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Form\Extension\Validator\ValidatorExtension;
use Symfony\Component\Validator\Validation;
use Symfony\Component\Form\Extension\Core\Type\FormType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\FormInterface;
use HIVE\HiveExtForm\Custom\Symfony\Component\Form\Extension\Core\Type\HtmlType;
use HIVE\HiveExtForm\Custom\Symfony\Component\Form\Extension\Core\Type\ReadonlyType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\MoneyType;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Form\Extension\Core\Type\PercentType;
use Symfony\Component\Form\Extension\Core\Type\SearchType;
use Symfony\Component\Form\Extension\Core\Type\UrlType;
use Symfony\Component\Form\Extension\Core\Type\RangeType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CountryType;
use Symfony\Component\Form\Extension\Core\Type\LanguageType;
use Symfony\Component\Form\Extension\Core\Type\LocaleType;
use Symfony\Component\Form\Extension\Core\Type\TimezoneType;
use Symfony\Component\Form\Extension\Core\Type\CurrencyType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\DateIntervalType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TimeType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\RadioType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\RepeatedType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\Extension\Core\Type\ButtonType;
use Symfony\Component\Form\Extension\Core\Type\ResetType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\PhpBridgeSessionStorage;
use DateTime;
use ReflectionClass;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\ContentObject\ContentObjectRenderer;

/***
 *
 * This file is part of the "hive_ext_form" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017
 *
 ***/

/**
 * The RenderController is the main Controller of "hive_ext_form" Extension for TYPO3 CMS.
 * The RenderController injects Repositories and Services, renders and validates multistep forms
 * using standard Extbase models and some Symfony components.
 *
 * @package HIVE\HiveExtForm\Controller
 */
class RenderController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    const EXTENSION_KEY = 'hive_ext_form';

    const DEFAULT_TRANSLATION_PATH = 'LLL:EXT:hive_ext_form/Resources/Private/Language/translation.xlf:';

    const VALIDATOR_TRANSLATION_PATH = 'LLL:EXT:hive_ext_form/Resources/Private/Language/vendor/symfony/validator/Resources/translations/validators.xlf:';

    const BASE64_ENCODE_SAFE = '-_';

    const BASE64_ENCODE_SPLIT = '--';

    /**
     * renderRepository
     *
     * @var \HIVE\HiveExtForm\Domain\Repository\RenderRepository
     * @inject
     */
    protected $renderRepository = null;

    /**
     * formRepository
     *
     * @var \HIVE\HiveExtForm\Domain\Repository\FormRepository
     * @inject
     */
    protected $formRepository = null;

    /**
     * constraintRepository
     *
     * @var \HIVE\HiveExtForm\Domain\Repository\ConstraintRepository
     * @inject
     */
    protected $constraintRepository = null;

    /**
     * translationService
     *
     * @var \HIVE\HiveExtForm\Service\TranslationService
     * @inject
     */
    protected $translationService = null;

    /**
     * unitService
     *
     * @var \HIVE\HiveExtForm\Service\UnitService
     * @inject
     */
    protected $unitService = null;

    /**
     * sessionService
     *
     * @var \HIVE\HiveExtForm\Service\SessionService
     * @inject
     */
    protected $sessionService = null;

    /**
     * storageService
     *
     * @var \HIVE\HiveExtForm\Service\StorageService
     * @inject
     */
    protected $storageService = null;

    /**
     * patternService
     *
     * @var \HIVE\HiveExtForm\Service\PatternService
     * @inject
     */
    protected $patternService = null;

    /**
     * mailService
     *
     * @var \HIVE\HiveExtForm\Service\MailService
     * @inject
     */
    protected $mailService = null;

    /**
     * renderAction creates forms from Extbase Models "Form", "Step"
     *
     * @throws \Exception
     */
    public function renderAction()
    {
        /*
         * Unique plugin uid
         */

        $iPluginUid = $this->configurationManager->getContentObject()->data['uid'];
        $aConst = $this->patternService->getConstArray();
        /*
         * current page uid
         */

        $iCurrentPageUid = $GLOBALS['TSFE']->page['uid'];
        $iSumFilesizeInStep = 0;
        /*
         * Handle post_max_size errors: PHP does remove $_POST and $_FILES in this case.
         * It seems $_REQUEST is still there, and isSubmitted() is true for each form!
         */

        $iSubmitCounter = 0;
        $bError = false;
        $bFiles = false;
        /*
         * 0: Show Form
         * 1: TODO
         * 2: TODO
         */

        $iCase = 0;
        $session = $this->sessionService->initializeSession();
        /*
         * Request object
         */

        $request = Request::createFromGlobals();
        $aRequestParameters = $request->request->all();
        if (count($aRequestParameters) > 0) {
            $this->sessionService->saveRequestParametersIntoSession($aRequestParameters, $session);
        }
        $aSettings = $this->settings;
        /*
         * Init storage
         * Create folders
         */

        if (!$this->storageService->initStorage($aSettings)) {
            throw new \Exception('Misconfiguration in Form / Server. Cannot init storage.');
        }
        $iTotalSteps = 1;
        $iRenderStep = 1;
        $sProcessNumber = null;
        /*
         * Create form related arrays
         */

        $aFormFactory = [];
        $aForm = [];
        $aFormView = [];
        $aFormViewError = [];
        $defaults = [];
        $oForm = null;
        if (array_key_exists('oHiveExtForm', $aSettings)) {
            /* @var \HIVE\HiveExtForm\Domain\Model\Form $oForm */
            $oForm = $this->formRepository->findByUid(intval($aSettings['oHiveExtForm']));
            if ($oForm != null) {
                /* @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtForm\Domain\Model\Step> $oSteps */
                $oSteps = $oForm->getStep();
                if ($oSteps != null) {
                    if ($oSteps->count() > 0) {
                        /*
                         * TODO Check if parameters from double opt in mail present for the current form
                         *
                         */

                        /*
                         * Set processNumber for current form
                         * Use processNumber to save fields and files
                         */

                        $sProcessNumberPattern = $this->patternService->getProcessNumberPattern();
                        $sProcessNumberSessionFieldName = str_replace([
                                $aConst['PROCESS_NUMBER_PLACEHOLDER__FORM_UID']
                            ],
                            [$oForm->getUid()], $sProcessNumberPattern
                        );
                        $sProcessNumber = $this->sessionService->getParameterFromSession($sProcessNumberSessionFieldName, $session);
                        if ($sProcessNumber == null || $sProcessNumber == '') {
                            $oNow = new DateTime('NOW');
                            $oDate = $oNow->format('ymdHi');
                            $sProcessNumberSessionFieldValue = $oDate . '_' . sha1(random_int(100000000, 999999999));
                            $bProcessNumber = $this->sessionService->saveParameterIntoSession($sProcessNumberSessionFieldName, $sProcessNumberSessionFieldValue, $session);
                            $sProcessNumber = $this->sessionService->getParameterFromSession($sProcessNumberSessionFieldName, $session);
                        }
                        if ($sProcessNumber != null && $sProcessNumber != '') {
                            $this->storageService->setProcessNumber($sProcessNumber);
                        } else {
                            throw new \Exception('Error in Form. Cannot save and/or get \'processNumber\'.');
                        }
                        $iTotalSteps = $oSteps->count();
                        /* @var int $i */
                        $i = 1;
                        /* @var \HIVE\HiveExtForm\Domain\Model\Step $oStep */
                        foreach ($oSteps as $oStep) {
                            $aFormViewError[$i] = [];
                            $aFormFactory[$i] = Forms::createFormFactoryBuilder()->addExtension(new ValidatorExtension(Validation::createValidator()))->getFormFactory();
                            /*
                             * Each step is a single form and needs a unique name.
                             * Therefore use 'createNamedBuilder()'.
                             */

                            /**
                             * <form name="$sFormName" ...>
                             *
                             * @var string $sFormName
                             *
                             */
                            $sFormPattern = $this->patternService->getFormPattern();
                            $sFormName = str_replace([
                                    substr($aConst['FORM_PLACEHOLDER__FORM_UID'], 1),
                                    substr($aConst['FORM_PLACEHOLDER__STEP_UID'], 1)
                                ],
                                [$oForm->getUid(), $oStep->getUid()], $sFormPattern
                            );
                            $aForm[$i] = $aFormFactory[$i]->createNamedBuilder($sFormName, FormType::class, $defaults)->getForm();
                            /* @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtForm\Domain\Model\Fieldset> $oFieldsets */
                            $oFieldsets = $oStep->getFieldset();
                            if ($oFieldsets != null) {
                                if ($oFieldsets->count() > 0) {
                                    foreach ($oFieldsets as $oFieldset) {
                                        /* @var \HIVE\HiveExtForm\Domain\Model\Fieldset $oFieldset */
                                        /* @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<\HIVE\HiveExtForm\Domain\Model\Field> $oFields */
                                        $oFields = $oFieldset->getField();
                                        if ($oFields != null) {
                                            if ($oFields->count() > 0) {
                                                /* @var \HIVE\HiveExtForm\Domain\Model\Field $oField */
                                                foreach ($oFields as $oField) {
                                                    $this->addFieldToForm($aForm[$i], $oForm, $oStep, $oFieldset, $oField, $session);
                                                    /**
                                                     * Hidden step variable
                                                     */
                                                    $aForm[$i]->add('__step', HiddenType::class, [
                                                        'data' => $i
                                                    ]);
                                                    if ($iTotalSteps > 1 && $i > 1) {
                                                        /**
                                                         * "<<" button if more than one step
                                                         */
                                                        $aForm[$i]->add('__prev', SubmitType::class, [
                                                            'label' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                                                                self::DEFAULT_TRANSLATION_PATH . 'tx_hiveextform.submit.prev', self::EXTENSION_KEY
                                                            )
                                                        ]);
                                                    }
                                                    /**
                                                     * "Submit" / ">>" button
                                                     */
                                                    $aForm[$i]->add('__next', SubmitType::class, [
                                                        'label' => $i == $iTotalSteps ? trim($oForm->getDescription()) == '' ? \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                                                            self::DEFAULT_TRANSLATION_PATH . 'tx_hiveextform.submit.send', self::EXTENSION_KEY
                                                        ) : $oForm->getDescription() : \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                                                            self::DEFAULT_TRANSLATION_PATH . 'tx_hiveextform.submit.next', self::EXTENSION_KEY
                                                        )
                                                    ]);
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            /*
                             * =========================================================================================
                             * Handle Request
                             * =========================================================================================
                             */

                            $aForm[$i]->handleRequest();
                            $aFormView[$i] = $aForm[$i]->createView();
                            if ($aForm[$i]->isSubmitted()) {
                                $iSubmitCounter += 1;
                                // Validate step
                                $iSubmittedStep = intval($aFormView[$i]->children['__step']->vars['value']);
                                /**
                                 * "next" or "prev"
                                 */
                                if (array_key_exists('__prev', $aRequestParameters[key($aRequestParameters)])) {
                                    $iRenderStep = $iSubmittedStep - 1;
                                } else {
                                    /**
                                     * "next" or "submit"
                                     */
                                    if ($iSubmittedStep < $iTotalSteps) {
                                        $iRenderStep = $iSubmittedStep + 1;
                                    } else {
                                        $iRenderStep = $iSubmittedStep;
                                    }
                                }
                                /*
                                 * Check for errors
                                 *
                                 * TODO Understand how Symfony handles translations and change our shitty translation handling in translationService!
                                 *
                                 * Get error message translations by (Symfony internal) error codes.
                                 */

                                /* @var \Symfony\Component\Form\FormView $aFormView */
                                /* @var \Symfony\Component\Form\Form\FormView $oFormFormView */
                                foreach ($aFormView[$i]->children as $oFormFormView) {
                                    if (array_key_exists('errors', $oFormFormView->vars)) {
                                        /* @var \Symfony\Component\Form\FormErrorIterator $oFormErrorIterator */
                                        $oFormErrorIterator = $oFormFormView->vars['errors'];
                                        $iFormErrorIteratorCount = $oFormErrorIterator->count();
                                        if ($iFormErrorIteratorCount > 0) {
                                            $bError = true;
                                            /* @var \Symfony\Component\Form\FormError $oFormError */
                                            foreach ($oFormErrorIterator as $oFormError) {
                                                /*
                                                 * Error messages must be translated!
                                                 *
                                                 * $formView gives us only english (en) error messages.
                                                 * We have to do some crazy shit to translate the messages below ;).
                                                 *
                                                 * Use translationService to do the crazy shit
                                                 */

                                                $sErrorMessage = $this->translationService->getErrorMessageForFormError($oFormError);
                                                $aFormViewError[$i][] = [
                                                    'label' => $oFormFormView->vars['label'],
                                                    'message' => $sErrorMessage
                                                ];
                                            }
                                            /*
                                             * Render same step with error message!
                                             */

                                            $iRenderStep = $iSubmittedStep;
                                        }
                                        /*
                                         * =============================================================================
                                         * Handle file specific errors
                                         * =============================================================================
                                         */

                                        if (array_key_exists('block_prefixes', $oFormFormView->vars)) {
                                            if ($oFormFormView->vars['block_prefixes'][1] == 'file') {
                                                $bFiles = true;
                                                if ($oFormFormView->vars['data'] != null) {
                                                    /*
                                                     * Handle file specific errors
                                                     */

                                                    if (array_key_exists('constraints', $oFormFormView->vars['attr'])) {
                                                        /*
                                                         * -------------------------------------------------------------
                                                         * Check MimeType
                                                         * -------------------------------------------------------------
                                                         */

                                                        if (array_key_exists('mimeTypes',
                                                            $oFormFormView->vars['attr']['constraints']
                                                        )) {
                                                            if (strpos($oFormFormView->vars['attr']['constraints']['mimeTypes'],
                                                                $oFormFormView->vars['data']['type']
                                                            ) === false) {
                                                                /*
                                                                 * MimeType not allowed => Error
                                                                 */

                                                                $bError = true;
                                                                $sErrorMessage = str_replace('{{ type }}',
                                                                    \HIVE\HiveExtForm\Custom\FriendlyMime::getFriendlyName($oFormFormView->vars['data']['type']),
                                                                    $this->translationService->getLocalizedErrorMessageByErrorcode('file_mime_type')
                                                                );
                                                                $aAllowedMimeTypes = explode(',',
                                                                    $oFormFormView->vars['attr']['constraints']['mimeTypes']
                                                                );
                                                                foreach ($aAllowedMimeTypes as $iKey => $sValue) {
                                                                    $aAllowedMimeTypes[$iKey] = \HIVE\HiveExtForm\Custom\FriendlyMime::getFriendlyName($sValue);
                                                                }
                                                                $sErrorMessage = str_replace('{{ types }}',
                                                                    implode(', ', $aAllowedMimeTypes), $sErrorMessage
                                                                );
                                                                $aFormViewError[$i][] = [
                                                                    'label' => $oFormFormView->vars['label'],
                                                                    'message' => $sErrorMessage
                                                                ];
                                                            }
                                                        }
                                                        /*
                                                         * -------------------------------------------------------------
                                                         * Check maxSize against maxSize
                                                         * -------------------------------------------------------------
                                                         */

                                                        if (array_key_exists('maxSize',
                                                            $oFormFormView->vars['attr']['constraints']
                                                        )) {
                                                            $iMaxSizeInBytes = $oFormFormView->vars['attr']['constraints']['maxSize'];
                                                            $sMaxSize = $oFormFormView->vars['attr']['constraints']['maxSizeString'];
                                                            if (intval($oFormFormView->vars['data']['size']) > $iMaxSizeInBytes) {
                                                                /*
                                                                 * MimeType too big => Error
                                                                 */

                                                                $bError = true;
                                                                $sErrorMessage = str_replace([
                                                                        '{{ limit }} {{ suffix }}',
                                                                        '{{ limit }}',
                                                                        '{{ suffix }}'
                                                                    ],
                                                                    ['{{ limit }}{{ suffix }}', $sMaxSize, ''],
                                                                    $this->translationService->getLocalizedErrorMessageByErrorcode('file_size')
                                                                );
                                                                $aFormViewError[$i][] = [
                                                                    'label' => $oFormFormView->vars['label'],
                                                                    'message' => $sErrorMessage
                                                                ];
                                                            }
                                                        }
                                                    }
                                                    /*
                                                     * -----------------------------------------------------------------
                                                     * Check fileSize against ini setting upload_max_filesize and other
                                                     * php standard error (always)
                                                     * -----------------------------------------------------------------
                                                     */

                                                    $sUploadMaxFileSize = ini_get('upload_max_filesize');
                                                    switch ($oFormFormView->vars['data']['error']) {
                                                        case UPLOAD_ERR_INI_SIZE:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'The uploaded file exceeds the upload_max_filesize (' . $sUploadMaxFileSize . ') directive in php.ini'
                                                            ];
                                                            break;
                                                        case UPLOAD_ERR_FORM_SIZE:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'The uploaded file exceeds the MAX_FILE_SIZE directive that was specified in the HTML form'
                                                            ];
                                                            break;
                                                        case UPLOAD_ERR_PARTIAL:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'The uploaded file was only partially uploaded'
                                                            ];
                                                            break;
                                                        case UPLOAD_ERR_NO_FILE:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'No file was uploaded'
                                                            ];
                                                            break;
                                                        case UPLOAD_ERR_NO_TMP_DIR:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'Missing a temporary folder'
                                                            ];
                                                            break;
                                                        case UPLOAD_ERR_CANT_WRITE:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'Failed to write file to disk'
                                                            ];
                                                            break;
                                                        case UPLOAD_ERR_EXTENSION:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'File upload stopped by extension'
                                                            ];
                                                            break;
                                                        case UPLOAD_ERR_OK:    break;
                                                        default:    $bError = true;
                                                            $aFormViewError[$i][] = [
                                                                'label' => $oFormFormView->vars['label'],
                                                                'message' => 'Unknown upload error'
                                                            ];
                                                            break;
                                                    }
                                                    /*
                                                     * -----------------------------------------------------------------
                                                     * Sum (filesize of all files) += current filesize (bytes)
                                                     * -----------------------------------------------------------------
                                                     */

                                                    $iUploadMaxFileSizeInBytes = $this->unitService->getSizeInBytes($sUploadMaxFileSize);
                                                    $iSumFilesizeInStep += $iUploadMaxFileSizeInBytes;
                                                }
                                            }
                                        }
                                    }
                                }
                                if (!$bError) {
                                    if ($bFiles) {

                                    }
                                }
                                if ($bError and $bFiles) {

                                }
                                if ($bError) {
                                    /*
                                     * Render same step with error message!
                                     */

                                    $iRenderStep = $iSubmittedStep;
                                }
                            }
                            $i++;
                        }
                    }
                }
                if ($iSubmitCounter > 0) {
                    /*
                     * Handle post_max_size errors: PHP does remove $_POST and $_FILES in this case. It seems $_REQUEST is still there, and isSubmitted() is true for each step/form!
                     * Use this fact to render current step; Save current step temporary??? If multiple steps and all steps submitted true => handle error
                     */

                    if (isset($_POST) and count($_POST) == 0) {
                        /*
                         * symfony cannot handle $iRenderStep in this case correctly, so reset to 1
                         */

                        $iRenderStep = 1;
                        /*
                         * Generate error message
                         */

                        $bError = true;
                        $sErrorMessage = 'Oops! File(s) too big to upload! Allowed filesize for all files: ' . ini_get('post_max_size');
                        $aFormViewError[$iRenderStep][] = [
                            'label' => 'Files',
                            'message' => $sErrorMessage
                        ];
                    } else {
                        if ($iSubmitCounter == 1) {
                            if (!$bError) {
                                /*
                                 * Init storage for submitted form / step
                                 * Create folders for submitted form /step
                                 * Move uploaded files for form / step
                                 */

                                if (!$this->storageService->initStorageForSubmittedForm($aSettings, $aFormView[$iSubmittedStep]->vars['name'])) {
                                    throw new \Exception('Misconfiguration in Form / Server. Cannot init storage for submitted form.');
                                }
                                if ($bFiles) {
                                    /*
                                     * Move uploaded file(s) and save filenames to active session
                                     */

                                    $bMoveUploadedFiles = $this->storageService->moveUploadedFiles($aFormView[$iSubmittedStep], $aSettings);
                                }
                                /*
                                 * Save values from active session
                                 */

                                if ($this->storageService->getProcessNumber() != null) {
                                    $this->storageService->saveCurrentSessionViaFormViewIntoFile($aFormView, $iSubmittedStep, $this->storageService->getProcessNumber());
                                } else {
                                    throw new \Exception('Fatal error in Form. No \'processNumber\' present. This should never happen.');
                                }
                                /*
                                 * Submitted last step
                                 */

                                if ($iSubmittedStep == $iTotalSteps) {
                                    $iCase = 1;
                                }
                            }
                        }
                    }
                }
                $aRequestArguments = $this->request->getArguments();
                if (isset($aRequestArguments['token']) and isset($aRequestArguments['element']) and $aRequestArguments['element'] == $iPluginUid) {
                    $iCase = 2;
                }
                switch ($iCase) {
                    case 1:
                        /*
                         * Send Mail(s)
                         */

                        if ($oForm != null and $oForm instanceof \HIVE\HiveExtForm\Domain\Model\Form) {
                            if ($oForm->getMailToSenderConfirm() != null) {
                                /*
                                 * Create double-opt-in link
                                 */

                                $aPluginConfiguration = [
                                    'pluginName' => 'tx_hiveextform_hiveextformrenderrender',
                                    'controller' => 'Render',
                                    'action' => 'render'
                                ];
                                $aPluginArguments = [
                                    'token' => $sProcessNumber,
                                    'element' => $iPluginUid,
                                    'controller' => $aPluginConfiguration['controller'],
                                    'action' => $aPluginConfiguration['action']
                                ];
                                if (!empty($oForm->getFieldSenderMail()) and GeneralUtility::validEmail($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail()))) {
                                    $aPluginArguments['token1'] = $this->base64_encode_safe($this->sessionService->getParameterFromActiveSession($oForm->getFieldSenderMail()));
                                }
                                $uriBuilder = $this->controllerContext->getUriBuilder();
                                $uriBuilder->reset();
                                $uriBuilder->setTargetPageUid($iCurrentPageUid);
                                $uriBuilder->setCreateAbsoluteUri(true);
                                $uriBuilder->setArguments([
                                    $aPluginConfiguration['pluginName'] => $aPluginArguments
                                ]);
                                $uriBuilder->setSection('p' . $iPluginUid);
                                $sUrl = $uriBuilder->build();
                                $bMailToSenderConfirm = $this->mailService->sendMailToSenderConfirm($oForm, $sUrl, $oForm->getMailToSenderConfirm());
                                /*
                                 * Remove session values for current form
                                 */

                                $bResetSession = $this->sessionService->resetActiveSessionForGivenForm($oForm->getUid());
                                $this->view->assign('doubleOptIn', true);
                                $this->view->assign('oForm', $oForm);
                            } else {
                                /*
                                 * Move Submitted values and files
                                 */

                                $aMirror = $this->storageService->mirrorSubmittedDataAfterConfirmation($sProcessNumber, $aSettings);
                                if ($aMirror[0] == 1) {

                                    /*
                                     * Pass value(s) to sendMailToSender and sendMailToReceiver
                                     */
                                    $aValue = [];
                                    $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'PassValueWithoutConfirmation', [
                                        &$aValue
                                    ]);

                                    $sCustomTextSubmit = "";
                                    $sTextSubmit = $oForm->getTextSubmit();
                                    $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'CustomTextSubmitWithoutConfirmation', [
                                        &$aValue,
                                        &$sCustomTextSubmit,
                                        $sTextSubmit
                                    ]);
                                    $this->view->assign('sCustomTextSubmit', $sCustomTextSubmit);

                                    if ($oForm->getMailToSender() != null) {
                                        /*
                                         * Send mail to sender
                                         */

                                        $bMailToSender = $this->mailService->sendMailToSender($oForm, $oForm->getMailToSender(), '', $sProcessNumber, $aSettings, $aValue);
                                    }
                                    if ($oForm->getMailToReceiver() != null) {
                                        /*
                                         * Send mail to Receiver
                                         */

                                        $bMailtoReceiver = $this->mailService->sendMailToReceiver($oForm, $sProcessNumber, $oForm->getMailToReceiver(), $aSettings, $aValue);
                                    }
                                    /*
                                     * Remove session values for current form
                                     */

                                    $bResetSession = $this->sessionService->resetActiveSessionForGivenForm($oForm->getUid());
                                }
                                $this->view->assign('doubleOptIn', false);
                                $this->view->assign('confirm', $aMirror);
                                $this->view->assign('oForm', $oForm);
                            }
                        }
                        break;
                    case 2:
                        /*
                         * Confirm Double Opt-In
                         * Move Submitted values and files
                         */

                        $aMirror = $this->storageService->mirrorSubmittedDataAfterConfirmation($aRequestArguments['token'], $aSettings);
                        $this->view->assign('confirm', $aMirror);
                        $this->view->assign('oForm', $oForm);
                        if ($aMirror[0] == 1) {

                            /*
                             * Pass value(s) to sendMailToSender and sendMailToReceiver
                             */
                            $aValue = [];
                            $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'PassValueWithConfirmation', [
                                &$aValue
                            ]);

                            if ($oForm->getMailToSender() != null) {
                                /*
                                 * Send mail to sender
                                 */

                                $bMailToSender = $this->mailService->sendMailToSender($oForm, $oForm->getMailToSender(), $this->base64_decode_safe($aRequestArguments['token1']), $aRequestArguments['token'], $aSettings, $aValue);
                            }
                            if ($oForm->getMailToReceiver() != null) {
                                /*
                                 * Send mail to Receiver
                                 */

                                $bMailtoReceiver = $this->mailService->sendMailToReceiver($oForm, $aRequestArguments['token'], $oForm->getMailToReceiver(), $aSettings, $aValue);
                            }
                        }
                        break;
                    default:
                        /*
                         * Render Form
                         */

                        $sPostMaxSize = ini_get('post_max_size');
                        $iPostMaxSizeInBytes = $this->unitService->getSizeInBytes($sPostMaxSize);
                        $this->view->assign('processNumber', $sProcessNumber);
                        $this->view->assign('form', $aFormView[$iRenderStep]);
                        $this->view->assign('post_max_size_ini', $sPostMaxSize);
                        $this->view->assign('post_max_size', $iPostMaxSizeInBytes);
                        $this->view->assign('errors', $aFormViewError[$iRenderStep]);
                        $this->view->assign('localizations_for_java_script', $this->translationService->getLocalizationsForJavaScript());
                }
            }
        }
        $this->view->assign('case', $iCase);
        $this->view->assign('pluginUid', $iPluginUid);
    }

    /**
     * @param array $aArray
     * @param string $sClean
     * @return array
     */
    private function cleanUpArray($aArray = [], $sClean = '__')
    {
        $aCleanArray = $aArray;
        foreach ($aCleanArray as $key => $val) {
            if (strpos($key, $sClean) !== false) {
                unset($aCleanArray[$key]);
            } else {
                if (is_array($val)) {
                    $aCleanArray[$key] = $this->cleanUpArray($val);
                } else {
                    $aCleanArray[$key] = $val;
                }
            }
        }
        return $aCleanArray;
    }

    /**
     * @param $sStringToEncode
     */
    private function base64_encode_safe($sStringToEncode)
    {
        return str_replace(['=', '=='], [self::BASE64_ENCODE_SAFE, self::BASE64_ENCODE_SAFE . self::BASE64_ENCODE_SAFE], base64_encode($sStringToEncode));
    }

    /**
     * @param $sStringToDecode
     */
    private function base64_decode_safe($sStringToDecode)
    {
        return base64_decode(str_replace([self::BASE64_ENCODE_SAFE, self::BASE64_ENCODE_SAFE . self::BASE64_ENCODE_SAFE], ['=', '=='], $sStringToDecode));
    }

    /**
     * @param \Symfony\Component\Form\Form $oFormForm
     * @param \HIVE\HiveExtForm\Domain\Model\Form $oForm
     * @param $oStep
     * @param $oFieldset
     * @param $oField
     * @param $oSession
     */
    private function addFieldToForm($oFormForm, $oForm, $oStep, $oFieldset, $oField, $oSession)
    {
        $aConst = $this->patternService->getConstArray();
        $iFormUid = $oForm->getUid();
        $sFormTitle = $oForm->getTitle();
        $iFieldUid = $oField->getUid();
        $sFieldTitle = $oField->getTitle();
        $oConstraint = $this->constraintRepository->getOneByFormAndFieldUid($iFormUid, $iFieldUid);
        /* @var \HIVE\HiveExtForm\Domain\Model\Constraint[] $aConstraint */
        $aConstraint = $oConstraint->toArray();
        if (count($aConstraint) > 1) {
            throw new \Exception("Misconfiguration in Form '{$sFormTitle}'. More than one Constraint for form '{$sFormTitle}' [{$iFormUid}] and field '{$sFieldTitle}' [{$iFieldUid}].");
        }
        $aConstraints = [];
        $aAttr = ['fieldset' => $oFieldset->getUid()];
        $sMandatory = '';
        $bMandatory = false;
        if (count($aConstraint) > 0) {
            if ($aConstraint[0]->getMandatory()) {
                $sMandatory = $aConst['FIELD__MANDATORY'];
                //                $aConstraints[] = new Assert\NotNull();
                $aConstraints[] = new Assert\NotBlank();
                $aAttr['help'] = \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                    self::DEFAULT_TRANSLATION_PATH . 'tx_hiveextform.label.mandatory', self::EXTENSION_KEY
                );
                $bMandatory = true;
            }
        }

        /**
         * Add help text to $attr['help']
         */
        $sHelpText = $oField->getHelpText();
        if (trim($sHelpText) != "") {
            if (key_exists('help',$aAttr)) {
                $aAttr['help'] .= " - " . $sHelpText;
            } else {
                $aAttr['help'] = $sHelpText;
            }
        }

        /**
         * Add field to form/step
         */
        $sKeyForm = $oForm->getUid();
        $sKeyStep = $oStep->getUid();
        $sKeyFieldset = $oFieldset->getUid();
        $sKeyField = $oField->getUid();
        /*
         * Do not change schema for field names
         */

        $sFieldPattern = $this->patternService->getFieldPattern();
        $sKey = str_replace([
                $aConst['FIELD_PLACEHOLDER__FORM_UID'],
                $aConst['FIELD_PLACEHOLDER__STEP_UID'],
                $aConst['FIELD_PLACEHOLDER__FIELDSET_UID'],
                $aConst['FIELD_PLACEHOLDER__FIELD_UID']
            ],
            [
                $sKeyForm,
                $sKeyStep,
                $sKeyFieldset,
                $sKeyField
            ],
            $sFieldPattern
        );
        $aSession = $oSession->all();
        $sData = array_key_exists($sKey, $aSession) ? $aSession[$sKey] : '';
        $sLabel = trim($oField->getRichTextLabel()) != '' ? trim($this->formatRTE($oField->getRichTextLabel())) : $oField->getTitle();
        $aAttr['placeholder'] = $sLabel;
        $sCurrentSessionValue = $this->sessionService->getParameterFromActiveSession($sKey);
        if ($sCurrentSessionValue == null) {
            $aAttr['session'] = '';
        } else {
            $aAttr['session'] = strlen($sCurrentSessionValue) > 50 ? substr($sCurrentSessionValue, 0, 50) . '...' : $sCurrentSessionValue;
        }
        switch ($oField->getType()) {
            case 'TextType':
                $oFormForm->add($sKey, TextType::class, [
                    'data' => $sData,
                    'label' => $sLabel . $sMandatory,
                    'required' => $bMandatory ? true : false,
                    'attr' => $aAttr,
                    'constraints' => $aConstraints
                ]);
                break;
            case 'TextareaType':
                $oFormForm->add($sKey, TextareaType::class, [
                    'data' => $sData,
                    'label' => $sLabel . $sMandatory,
                    'required' => $bMandatory ? true : false,
                    'attr' => $aAttr,
                    'constraints' => $aConstraints
                ]);
                break;
            case 'ReadonlyType':
                $sPleaseOverrideValue = '';
                $aAttr['value'] = $sPleaseOverrideValue;
                /*
                 * ReadonlyType Slot
                 */

                $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideReadonlyType', [
                    $sKey,
                    &$aAttr,
                    &$sLabel
                ]);
                $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideReadonlyTypeV1', [
                    $sKey,
                    &$aAttr,
                    &$sLabel,
                    $sData
                ]);

                /*
                 * Set $sData already at the first form load
                 *
                 * Override with value from signal if needed
                 * Could happen if form is used on a page with a
                 * show action and hidden value should be different
                 * for different objects
                 */
                if ($sData != '') {
                    if ($sData != $aAttr['value']) {
                        $sData = $aAttr['value'];
                    }
                } else {
                    $sData = $aAttr['value'];
                }
                /*
                 * Set session value already at the first form load !
                 */
                if ($sData != "") {
                    $this->sessionService->saveParameterIntoActiveSession($sKey, $sData);
                    $sCurrentSessionValue = $this->sessionService->getParameterFromActiveSession($sKey);
                    if ($sCurrentSessionValue == null) {
                        $aAttr['session'] = '';
                    } else {
                        $aAttr['session'] = strlen($sCurrentSessionValue) > 50 ? substr($sCurrentSessionValue, 0, 50) . '...' : $sCurrentSessionValue;
                    }
                }

                $oFormForm->add($sKey, ReadonlyType::class, [
                    'data' => $sData,
                    'label' => $sLabel,
                    'attr' => $aAttr
                ]);
                break;
            case 'HiddenType':
                $sPleaseOverrideValue = '';
                $aAttr['value'] = $sPleaseOverrideValue;
                /*
                 * HiddenType Slot
                 */

                /*
                 * signalSlotDispatcher
                 */

                $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideHiddenType', [
                    $sKey,
                    &$aAttr,
                    &$sLabel
                ]);
                $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'OverrideHiddenTypeV1', [
                    $sKey,
                    &$aAttr,
                    &$sLabel,
                    $sData
                ]);

                /*
                 * Set $sData already at the first form load
                 * 
                 * Override with value from signal if needed
                 * Could happen if form is used on a page with a
                 * show action and hidden value should be different
                 * for different objects
                 */
                if ($sData != '') {
                    if ($sData != $aAttr['value']) {
                        $sData = $aAttr['value'];
                    }
                } else {
                    $sData = $aAttr['value'];
                }
                /*
                 * Set session value already at the first form load !
                 */
                if ($sData != "") {
                    $this->sessionService->saveParameterIntoActiveSession($sKey, $sData);
                    $sCurrentSessionValue = $this->sessionService->getParameterFromActiveSession($sKey);
                    if ($sCurrentSessionValue == null) {
                        $aAttr['session'] = '';
                    } else {
                        $aAttr['session'] = strlen($sCurrentSessionValue) > 50 ? substr($sCurrentSessionValue, 0, 50) . '...' : $sCurrentSessionValue;
                    }
                }

                $oFormForm->add($sKey, HiddenType::class, [
                    'data' => $sData,
                    'label' => $sLabel,
                    'attr' => $aAttr
                ]);
                break;
            case 'EmailType':
                $aConstraints[] = new Assert\Email();
                $oFormForm->add($sKey, EmailType::class, [
                    'data' => $sData,
                    'label' => $sLabel . $sMandatory,
                    'required' => $bMandatory ? true : false,
                    'attr' => $aAttr,
                    'constraints' => $aConstraints
                ]);
                break;
            case 'CheckboxType':
                $bData = array_key_exists($sKey, $aSession) ? $aSession[$sKey] == '1' ? true : false : false;
                $oFormForm->add($sKey, CheckboxType::class, [
                    'data' => $bData,
                    'value' => $bData ? '1' : '',
                    'label' => $sMandatory . $sLabel,
                    'attr' => $aAttr,
                    'required' => $bMandatory ? true : false,
                    'constraints' => $bMandatory ? $aConstraints[] = new Assert\IsTrue() : $aConstraints
                ]);
                /*
                 * Workaround for empty checkboxes
                 *
                 * See also: Template and sessionService
                 */

                $sEmptyCheckboxKey = $sKey . '--empty_checkbox';
                $oFormForm->add($sEmptyCheckboxKey, HiddenType::class, [
                    'data' => '0'
                ]);
                break;
            case 'ChoiceType':
                $aChoices = [];

                /*
                 * PHP_EOL and line endings in DB may differ
                 *
                 * So => Replace different line endings from DB
                 * and use PHP_EOL instead
                 */
                $sOptionsTmp = str_replace(["\r\n", "\r", "\n"], ["||||", "||||", "||||"], $oField->getOptions());
                $sOptionsTmp = str_replace("||||", PHP_EOL, $sOptionsTmp);

                $sSeparator = PHP_EOL;
                $line = strtok($sOptionsTmp, $sSeparator);
                $group = '';
                $sDefaultData = '';
                while ($line !== false) {
                    if (strpos($line, '---') !== false) {
                        $group = str_replace('---', '', $line);
                        if ($group != '') {
                            $aChoices[$group] = [];
                        }
                    } else {
                        $aOption = explode('|', $line);
                        if (count($aOption) == 3 && strpos($aOption[2], "selected") !== false) {
                            $sDefaultData = $aOption[0];
                        }
                        if ($group != '') {
                            if (count($aOption) > 1) {
                                $aChoices[$group][addslashes($aOption[1])] = $aOption[0];
                            } else {
                                $aChoices[$group][addslashes($aOption[0])] = $aOption[0];
                            }
                        } else {
                            if (count($aOption) > 1) {
                                $aChoices[addslashes($aOption[1])] = $aOption[0];
                            } else {
                                $aChoices[addslashes($aOption[0])] = $aOption[0];
                            }
                        }
                    }
                    $line = strtok($sSeparator);
                }

                /*
                 * Set default value if the form is loaded initially
                 */
                if ($sData == "" && $sDefaultData != "") {
                    $sData = $sDefaultData;
                    /*
                     * If form is sent and select field is not mandatory
                     * empty values are allowed if the user selects the
                     * empty value => Override default value
                     */
                    if (isset($_POST) && ! $bMandatory) {
                        $sData == "";
                    }
                }
                $oFormForm->add($sKey, ChoiceType::class, [
                    'choices' => $aChoices,
                    'data' => $sData,
                    'label' => $sLabel . $sMandatory,
                    'attr' => $aAttr,
                    'required' => $bMandatory ? true : false,
                    'constraints' => $aConstraints
                ]);
                break;
            case 'CountryType':
                $oClass = new ReflectionClass('\\HIVE\\HiveExtForm\\Service\\TranslationService');
                $aConst = $oClass->getConstants();
                $sPlaceholder = $aConst['DEFAULT_TRANSLATION_PATH'];
                $aPreferredChoices = ['DE', 'CH', 'AT'];
                /*
                 * signalSlotDispatcher
                 */

                $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'SetPreferredChoicesForCountryType', [
                    $sKey,
                    &$aPreferredChoices
                ]);
                $oFormForm->add($sKey, CountryType::class, [
                    'data' => $sData,
                    'preferred_choices' => $aPreferredChoices,
                    'placeholder' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                        $sPlaceholder . 'tx_hiveextform.countryType.placeholder',
                        self::EXTENSION_KEY
                    ),
                    'label' => $sLabel . $sMandatory,
                    'attr' => $aAttr,
                    'required' => $bMandatory ? true : false,
                    'constraints' => $aConstraints
                ]);
                break;
            case 'LanguageType':
                $oClass = new ReflectionClass('\\HIVE\\HiveExtForm\\Service\\TranslationService');
                $aConst = $oClass->getConstants();
                $sPlaceholder = $aConst['DEFAULT_TRANSLATION_PATH'];
                $aPreferredChoices = ['de', 'de_CH', 'de_AT'];
                /*
                 * signalSlotDispatcher
                 */

                $this->signalSlotDispatcher->dispatch(__CLASS__, __FUNCTION__ . 'SetPreferredChoicesForLanguageType', [
                    $sKey,
                    &$aPreferredChoices
                ]);
                $oFormForm->add($sKey, LanguageType::class, [
                    'data' => $sData,
                    'preferred_choices' => $aPreferredChoices,
                    'placeholder' => \TYPO3\CMS\Extbase\Utility\LocalizationUtility::translate(
                        $sPlaceholder . 'tx_hiveextform.languageType.placeholder',
                        self::EXTENSION_KEY
                    ),
                    'label' => $sLabel . $sMandatory,
                    'attr' => $aAttr,
                    'required' => $bMandatory ? true : false,
                    'constraints' => $aConstraints
                ]);
                break;
            case 'FileType':
                /*
                 * File contraints must be handled later, cause I just don't understand
                 * how to do that without a symfony model.
                 *
                 * TODO Find a way to do file constraints right
                 */

                if (count($aConstraint) > 0) {
                    if (count($aConstraint) > 1) {
                        throw new \Exception("Misconfiguration in Form '{$sFormTitle}'. More than one Constraint for form '{$sFormTitle}' [{$iFormUid}] and field '{$sFieldTitle}' [{$iFieldUid}].");
                    }
                    /*
                     * Save constraints to attr array until we find a better way and
                     * handle file specific constraints later
                     *
                     * // Not working right now
                     * $aConstraints[] = new Assert\File(array(
                     *      'maxSize' => '10k',
                     *      'mimeTypes' => array(
                     *          'application/pdf',
                     *          'application/x-pdf',
                     *      ),
                     *  ));
                     */

                    if ($aConstraint[0]->getMimeType() != '') {
                        $aAttr['constraints']['mimeTypes'] = str_replace(',', ', ', $aConstraint[0]->getMimeType());
                    }
                    if ($aConstraint[0]->getMaxFileSize() > 0) {
                        $sMaxSize = $aConstraint[0]->getMaxFileSize() . $aConstraint[0]->getMaxFileSizeUnit();
                        $iMaxSizeInBytes = $this->unitService->getSizeInBytes($sMaxSize);
                        $aAttr['constraints']['maxSize'] = $iMaxSizeInBytes;
                        $aAttr['constraints']['maxSizeString'] = $sMaxSize;
                    }
                }
                $oFormForm->add($sKey, FileType::class, [
                    'label' => $sLabel . $sMandatory,
                    'attr' => $aAttr,
                    'constraints' => $aConstraints,
                    'data_class' => 'Symfony\\Component\\HttpFoundation\\File\\File'
                ]);
                break;
            case 'Html':    $oFormForm->add($sKey, HtmlType::class, [
                    'data' => $sData,
                    'label' => $sLabel
                ]);
                break;
            default:    
        }
    }

    protected function formatRTE($str) {
        $contentObject = GeneralUtility::makeInstance(ContentObjectRenderer::class);
        $output = $contentObject->parseFunc($str, array(), '< lib.parseFunc_RTE');
        return $output;
    }
}
