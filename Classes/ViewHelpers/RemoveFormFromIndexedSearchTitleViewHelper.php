<?php
namespace HIVE\HiveExtForm\ViewHelpers;

/**
 * The MIT License (MIT)
 *
 * Copyright (c) 2015 Andreas Hafner, Dominik Hilser, Georg Kathan, Hendrik Krüger, Timo Bittner - hive GmbH
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 **/

use DOMDocument;
use TYPO3\CMS\Fluid\Core\ViewHelper\AbstractTagBasedViewHelper;

class RemoveFormFromIndexedSearchTitleViewHelper extends AbstractTagBasedViewHelper {

    /**
     * @param string $title
     * @return mixed
     *
     * * Use e.g.:
     * <html xmlns:hiveextform="http://typo3.org/ns/HIVE/HiveExtForm/ViewHelpers">
     * ...
     * <hiveextform:removeFormFromIndexedSearchTitle title="{row.title}"/>
     * ...
     *
     */
    public function render($title) {

        // instantiate new `DOMDocument` object
        $dom = new DOMDocument();
        // load $html into `DOMDocument`
        $dom->loadHTML($title);
        // get all anchor elements
        $elements = $dom->getElementsByTagName('a');
        // iterate over anchors
        $href = $elements[0]->getAttribute('href');

        $aCleanHref = explode("hive_ext_form/", $href);
        $sCleanHref = $aCleanHref[0];

        return str_replace($href, $sCleanHref, $title);
    }
}