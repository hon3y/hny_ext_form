// var tx_hivecextform__totalSize = 0;

var iPostMaxSize = 0;
var sPostMaxSizeIni = '';
var bPostMaxSizeError = false;
var bAnyError = false;
var iSumFileSize = 0;
var aTxHivecextformBtnSubmit = document.getElementsByClassName("btn-submit");
var oTxHivecextformFileinput = new Object();

function tx_hivecextform__addClass(element, sClass) {
    var name, arr;
    name = sClass;
    arr = element.className.split(" ");
    if (arr.indexOf(name) == -1) {
        element.className += " " + name;
    }
}

function tx_hivecextform__removeClass(element, sClass) {
    var sRegex = '\\b' + sClass + '\\b';
    var oRegex = new RegExp(sRegex,"g");
    element.className = element.className.replace(oRegex, "");
}

function tx_hivecextform__insertAfter(newNode, referenceNode) {
    referenceNode.parentNode.insertBefore(newNode, referenceNode.nextSibling);
}

 var tx_hivecextform__interval = setInterval(function () {
    if (typeof hive_cfg_typoscript__windowLoad == 'undefined') {
    } else {
        if (typeof hive_cfg_typoscript__windowLoad == "boolean" && hive_cfg_typoscript__windowLoad) {

            clearInterval(tx_hivecextform__interval);

            var aTxHivecextform = document.getElementsByClassName('tx_hive-ext-form__form');
            if (aTxHivecextform.length > 0) {
                iPostMaxSize = aTxHivecextform[0].getAttribute('data-post-max-size');
                sPostMaxSizeIni = aTxHivecextform[0].getAttribute('data-post-max-size-ini');


                var aTxHivecextformFileinput = document.getElementsByClassName("form-control-file");
                if (aTxHivecextformFileinput.length > 0) {
                for (var ii = 0; ii < aTxHivecextformFileinput.length; ii++) {

                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')] = new Object();
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['size'] = 0;
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['type'] = '';
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['fileSizeAllowed'] = (aTxHivecextformFileinput[ii].getAttribute('data-max-size') == null ? null : parseInt(aTxHivecextformFileinput[ii].getAttribute('data-max-size')));
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['fileSizeAllowedHumanReadable'] = aTxHivecextformFileinput[ii].getAttribute('data-max-size-h');
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['mimeTypesAllowed'] = aTxHivecextformFileinput[ii].getAttribute('data-mime-types');
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['fileSizeError'] = [false, tx_hivecextform__lll_file_size.replace('{{ limit }} {{ suffix }}', aTxHivecextformFileinput[ii].getAttribute('data-max-size-h'))];

                    var sEM = tx_hivecextform__lll_file_mime_type.replace('{{ types }}', aTxHivecextformFileinput[ii].getAttribute('data-mime-types'));
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['mimeTypeError'] = [false, sEM];
                    oTxHivecextformFileinput[aTxHivecextformFileinput[ii].getAttribute('id')]['errorMessage'] = [];


                    aTxHivecextformFileinput[ii].addEventListener('change', function(){
                        var file = this.files[0];
                        var inputId = this.getAttribute('id');
                        // This code is only for demo ...
                        console.log("name : " + file.name);
                        console.log("size : " + file.size);
                        console.log("type : " + file.type);
                        console.log("date : " + file.lastModified);

                        oTxHivecextformFileinput[this.getAttribute('id')]['size'] = parseInt(file.size);
                        oTxHivecextformFileinput[this.getAttribute('id')]['type'] = file.type;

                        /**
                         * Check actual file size against allowed file size
                         */
                        if ( ! isNaN(oTxHivecextformFileinput[this.getAttribute('id')]['fileSizeAllowed'])
                            && oTxHivecextformFileinput[this.getAttribute('id')]['fileSizeAllowed'] != null) {
                            if (oTxHivecextformFileinput[this.getAttribute('id')]['size'] > oTxHivecextformFileinput[this.getAttribute('id')]['fileSizeAllowed']) {
                                oTxHivecextformFileinput[this.getAttribute('id')]['fileSizeError'][0] = true;
                            } else {
                                oTxHivecextformFileinput[this.getAttribute('id')]['fileSizeError'][0] = false;
                            }
                        }

                        /**
                         * Check actual mimeType against allowed mimeTypes
                         */
                        if (oTxHivecextformFileinput[this.getAttribute('id')]['mimeTypesAllowed'] != null) {
                            if (oTxHivecextformFileinput[this.getAttribute('id')]['mimeTypesAllowed'].indexOf(oTxHivecextformFileinput[this.getAttribute('id')]['type']) == -1) {
                                oTxHivecextformFileinput[this.getAttribute('id')]['mimeTypeError'][0] = true;
                            } else {
                                oTxHivecextformFileinput[this.getAttribute('id')]['mimeTypeError'][0] = false;
                            }
                        }

                        /**
                         * iSumFileSize
                         * @type {number}
                         */
                        iSumFileSize = 0;
                        for (var i = 0; i < Object.keys(oTxHivecextformFileinput).length; i++) {
                            var sObjectKey = Object.keys(oTxHivecextformFileinput)[i];
                            iSumFileSize += oTxHivecextformFileinput[sObjectKey]['size'];
                        }

                        /**
                         * If size of all files is bigger then post_max_size
                         *  => set bPostMaxSizeError true
                         * else
                         *  => set bPostMaxSizeError false (default)
                         */
                        if (iSumFileSize > iPostMaxSize) {
                            bPostMaxSizeError = true;
                        } else {
                            bPostMaxSizeError = false;
                        }

                        /**
                         * Show or hide error messages below file(s)
                         */
                        bAnyError = false;
                        for (var i = 0; i < Object.keys(oTxHivecextformFileinput).length; i++) {
                            var sObjectKey = Object.keys(oTxHivecextformFileinput)[i];
                            var sInnerHtml = "";
                            if (oTxHivecextformFileinput[sObjectKey]['fileSizeError'][0]) {
                                bAnyError = true;
                                sInnerHtml += '<div>' + oTxHivecextformFileinput[sObjectKey]['fileSizeError'][1] + '</div>';
                            }
                            if (oTxHivecextformFileinput[sObjectKey]['mimeTypeError'][0]) {
                                bAnyError = true;
                                sInnerHtml += '<div>' + oTxHivecextformFileinput[sObjectKey]['mimeTypeError'][1].replace('{{ type }}', oTxHivecextformFileinput[sObjectKey]['type']) + '</div>';
                            }
                            if (bPostMaxSizeError) {
                                bAnyError = true;
                                sInnerHtml += '<div>' + tx_hivecextform__lll_post_max_size.replace('{{ post-max-size }}', sPostMaxSizeIni) + '</div>';
                            }
                            var el = document.getElementById(sObjectKey);
                            var elIF = document.getElementById('invalid-feedback-' + sObjectKey);
                            if (el != null && elIF != null) {
                                elIF.innerHTML = sInnerHtml;
                                if (sInnerHtml != '') {
                                    tx_hivecextform__addClass(el, 'is-invalid');
                                } else {
                                    tx_hivecextform__removeClass(el, 'is-invalid');
                                }
                            }
                        }
                        if (bAnyError) {
                            /**
                             * Add disabled state from submit button(s)
                             */
                            if (aTxHivecextformBtnSubmit != null) {
                                for (var i = 0; i < aTxHivecextformBtnSubmit.length; i++) {
                                    aTxHivecextformBtnSubmit[i].setAttribute('disabled', 'disabled');
                                }
                            }
                        } else {
                            /**
                             * Remove disabled state from submit button(s)
                             */
                            if (aTxHivecextformBtnSubmit != null) {
                                for (var i = 0; i < aTxHivecextformBtnSubmit.length; i++) {
                                    aTxHivecextformBtnSubmit[i].removeAttribute('disabled');
                                }
                            }
                        }

                    });

                }
            }

            }
            // document.getElementById('fileinput').addEventListener('change', function(){
            //     var file = this.files[0];
            //     // This code is only for demo ...
            //     console.log("name : " + file.name);
            //     console.log("size : " + file.size);
            //     console.log("type : " + file.type);
            //     console.log("date : " + file.lastModified);
            // }, false);
//
//             var tx_hivecextform__fileinput = document.getElementsByClassName("form-control-file");
//             if (tx_hivecextform__fileinput != null) {
//                 for (var i = 0; i < tx_hivecextform__fileinput.length; i++) {
//                     tx_hivecextform__totalSize = tx_hivecextform__totalSize + tx_hivecextform__fileinput[i].size;
//                 }
//                 console.log(tx_hivecextform__totalSize);
//             }
        }
    }
});
// $("input:file").each(function(){
//     if($(this)[0].files[0]) {
//         totalSize = totalSize + $(this)[0].files[0].size;
//     }
// })